#include <gmock/gmock.h>

#include "Jeu/Jeu.hpp"

namespace {

using testing::Eq;

// test de la creation 
TEST(Jeu, creation) {
    // cree un jeu
    Grille g(10,40);
    Jeu partie(&g);
    // test le nb de lignes
    ASSERT_THAT(g.getNbLignes(), Eq(10));
    // test le nb de colonnes
    ASSERT_THAT(g.getNbColonnes(), Eq(40));
    ASSERT_THAT(g.getCase(5,5), Eq(false));
    partie.avance();
}

TEST(Jeu, annhilation1) {
    // cree un jeu
    Grille g(10,40);
    g.ajouteCellule(5,5);
    Jeu partie(&g);
    // test le nb de lignes
    ASSERT_THAT(g.getNbLignes(), Eq(10));
    // test le nb de colonnes
    ASSERT_THAT(g.getNbColonnes(), Eq(40));
    ASSERT_THAT(g.getCase(5,5), Eq(true));
    partie.avance();
    ASSERT_THAT(g.getCase(5,5), Eq(false));
}

TEST(Jeu, annhilation2) {
    // cree un jeu
    Grille g(10,40);
    g.ajouteCellule(5,5);
    g.ajouteCellule(5,6);
    g.ajouteCellule(5,4);
    Jeu partie(&g);
    // test le nb de lignes
    ASSERT_THAT(g.getNbLignes(), Eq(10));
    // test le nb de colonnes
    ASSERT_THAT(g.getNbColonnes(), Eq(40));
    ASSERT_THAT(g.getCase(5,5), Eq(true));
    partie.avance();
    ASSERT_THAT(g.getCase(5,5), Eq(true));
    ASSERT_THAT(g.getCase(5,4), Eq(false));
}

TEST(Jeu, reproduction) {
    // cree un jeu
    Grille g(10,40);
    g.ajouteCellule(5,5);
    g.ajouteCellule(5,6);
    g.ajouteCellule(6,5);
    Jeu partie(&g);
    partie.avance();
    ASSERT_THAT(g.getCase(6,6), Eq(true));
}

TEST(Jeu, isolement) {
    // cree un jeu
    Grille g(10,40);
    g.ajouteCellule(5,5);
    g.ajouteCellule(5,6);
    Jeu partie(&g);
    partie.avance();
    ASSERT_THAT(g.getCase(5,5), Eq(false));
}

TEST(Jeu, etouffement) {
    // cree un jeu
    Grille g(10,40);
    g.ajouteCellule(5,4);
    g.ajouteCellule(5,6);
    g.ajouteCellule(5,5);
    g.ajouteCellule(4,5);
    g.ajouteCellule(6,5);
    Jeu partie(&g);
    partie.avance();
    ASSERT_THAT(g.getCase(5,5), Eq(false));
}

TEST(Jeu, maintient) {
    // cree un jeu
    Grille g(10,40);
    g.ajouteCellule(5,4);
    g.ajouteCellule(5,6);
    g.ajouteCellule(5,5);
    g.ajouteCellule(4,5);
    Jeu partie(&g);
    partie.avance();
    ASSERT_THAT(g.getCase(5,5), Eq(true));
}

TEST(Jeu, blinker) {
    // cree un jeu
    Grille g(10,10);
    g.ajouteCellule(5,5);
    g.ajouteCellule(6,5);
    g.ajouteCellule(4,5);
    Jeu partie(&g);
    ASSERT_THAT(g.getCase(5,5), Eq(true));
    ASSERT_THAT(g.getCase(4,5), Eq(true));
    ASSERT_THAT(g.getCase(6,5), Eq(true));
    ASSERT_THAT(g.getCase(5,6), Eq(false));
    ASSERT_THAT(g.getCase(5,4), Eq(false));
    partie.avance();
    ASSERT_THAT(g.getCase(5,5), Eq(true));
    ASSERT_THAT(g.getCase(4,5), Eq(false));
    ASSERT_THAT(g.getCase(6,5), Eq(false));
    ASSERT_THAT(g.getCase(5,6), Eq(true));
    ASSERT_THAT(g.getCase(5,4), Eq(true));
    partie.avance();
    ASSERT_THAT(g.getCase(5,5), Eq(true));
    ASSERT_THAT(g.getCase(4,5), Eq(true));
    ASSERT_THAT(g.getCase(6,5), Eq(true));
    ASSERT_THAT(g.getCase(5,6), Eq(false));
    ASSERT_THAT(g.getCase(5,4), Eq(false));
    partie.avance();
    ASSERT_THAT(g.getCase(5,5), Eq(true));
    ASSERT_THAT(g.getCase(4,5), Eq(false));
    ASSERT_THAT(g.getCase(6,5), Eq(false));
    ASSERT_THAT(g.getCase(5,6), Eq(true));
    ASSERT_THAT(g.getCase(5,4), Eq(true));
}

TEST(Jeu, beacon) {
    // cree un jeu
    Grille g(20,20);
    g.ajouteCellule(10,10);
    g.ajouteCellule(9,10);
    g.ajouteCellule(10,11);
    g.ajouteCellule(9,11);
    g.ajouteCellule(11,9);
    g.ajouteCellule(12,9);
    g.ajouteCellule(11,8);
    g.ajouteCellule(12,8);
    Jeu partie(&g);
    ASSERT_THAT(g.getCase(10,10), Eq(true));
    ASSERT_THAT(g.getCase(11,9), Eq(true));
    ASSERT_THAT(g.getCase(11,8), Eq(true));
    ASSERT_THAT(g.getCase(12,8), Eq(true));
    ASSERT_THAT(g.getCase(9,11), Eq(true));
    partie.avance();
    ASSERT_THAT(g.getCase(10,10), Eq(false));
    ASSERT_THAT(g.getCase(11,9), Eq(false));
    ASSERT_THAT(g.getCase(11,8), Eq(true));
    ASSERT_THAT(g.getCase(12,8), Eq(true));
    ASSERT_THAT(g.getCase(9,11), Eq(true));
    partie.avance();
    ASSERT_THAT(g.getCase(10,10), Eq(true));
    ASSERT_THAT(g.getCase(11,9), Eq(true));
    ASSERT_THAT(g.getCase(11,8), Eq(true));
    ASSERT_THAT(g.getCase(12,8), Eq(true));
    ASSERT_THAT(g.getCase(9,11), Eq(true));
    partie.avance();
    ASSERT_THAT(g.getCase(10,10), Eq(false));
    ASSERT_THAT(g.getCase(11,9), Eq(false));
    ASSERT_THAT(g.getCase(11,8), Eq(true));
    ASSERT_THAT(g.getCase(12,8), Eq(true));
    ASSERT_THAT(g.getCase(9,11), Eq(true));
}

}  // namespace