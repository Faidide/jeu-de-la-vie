#include "Frame.h"

#include <wx/dcclient.h>
#include <wx/filedlg.h>
#include <wx/graphics.h>
#include <vector>
#include <sstream>
#include <iostream>
#include <cstring>

MyFrame::MyFrame(const wxString& title, const wxPoint& pos, const wxSize& size)
  : wxFrame(NULL, wxID_ANY, title, pos, size) {
  // crée le menu pour les fichiers
  wxMenu* menuFile = new wxMenu;
  // ajoute l'option pour creer une nouvelle grille
  menuFile->Append(ID_New, "&Nouveau\tCtrl-H",
                   "Creer une nouvelle grille de jeu de la vie");
  menuFile->Append(ID_Save, "&Enregistrer Sous\tCtrl-S",
                   "Enregistrez votre grille.");
  menuFile->Append(ID_Open, "&Ouvrir\tCtrl-O",
                   "Ouvrir une grille.");
  menuFile->AppendSeparator();
  // l'option pour quitter
  menuFile->Append(wxID_EXIT, "&Quitter\tCtrl-Q", "Quitter le logiciel");
  // le menu aide et à propos
  wxMenu* menuHelp = new wxMenu;
  menuHelp->Append(wxID_ABOUT, "&Notre Projet", "Qui sommes nous ?");
  // crée la barre de menu en haut
  wxMenuBar* menuBar = new wxMenuBar;
  // ajoute les précédentes sections
  menuBar->Append(menuFile, "&Fichier");
  menuBar->Append(menuHelp, "&A Propos");
  // initialise le menu
  SetMenuBar(menuBar);
  // crée une barre de status
  CreateStatusBar();
  // initialise la barre de status
  SetStatusText("Bienvenu dans le jeu de la vie !");

  // crée le panel
  wxImage::AddHandler(new wxPNGHandler);
  // y crée deux boutons
  wxBitmap play(wxT("../play.png"), wxBITMAP_TYPE_PNG);
  wxBitmap clear(wxT("../clear.png"), wxBITMAP_TYPE_PNG);
  //

  toolbar = CreateToolBar();
  toolbar->AddTool(ID_Play, wxT("Play/Pause"), play);
  toolbar->AddTool(ID_Clear, wxT("Effacer"), clear);
  toolbar->Realize();

  // Bind(ID_Play, wxEVT_COMMAND_TOOL_CLICKED,
  // wxCommandEventHandler(MyFrame::OnPause));

  // cree le dialogue de nouvelle grille
  newDialog = new NewDialog(wxT("Nouvelle Grille"), this);

  // cree les dessins de base
  // SetBackgroundColour(*wxWHITE);

  // cree la partie
  Grille* magrille = new Grille(10, 10);
  partieDuJeu = new Jeu(magrille);

  // cree et lance le timer
  timer = new wxTimer(this, TIMER_ID);
  timer->Start(1000);
  running = false;
}

// event handler de quand on quitte
void MyFrame::OnExit(wxCommandEvent& event) { Close(true); }

// envent handler du à propos
void MyFrame::OnAbout(wxCommandEvent& event) {
  wxMessageBox("Jeu de la vie par Antoine MILLON et Quentin FAIDIDE",
               "Fait pour un projet de L3 MI", wxOK | wxICON_INFORMATION);
}

// event handler de l'appel à une nouvelle grille
void MyFrame::OnNew(wxCommandEvent& event) {
  newDialog->Centre();
  modalID = newDialog->ShowModal();
}

void MyFrame::OnPaint(wxPaintEvent& event) {
  // initialize le dc
  wxPaintDC dc(this);
  // cree le graphicsContext
  wxGraphicsContext* gc = wxGraphicsContext::Create(dc);

  // recup la taille
  double xt, yt;
  gc->GetSize(&xt, &yt);

  taille_x = round(xt);
  taille_y = round(yt);

  int largeur_grille = partieDuJeu->getLargeur() * TAILLE_CELLULE;
  int hauteur_grille = partieDuJeu->getHauteur() * TAILLE_CELLULE;

  // calcule le padding à droite et au dessus pour centrer la grille
  if (hauteur_grille > taille_y) {
    topPadding = 0;
  } else {
    topPadding = (taille_y - hauteur_grille) / 2;
  }

  if (largeur_grille > taille_x) {
    leftPadding = 0;
  } else {
    leftPadding = (taille_x - largeur_grille) / 2;
  }

  // si ça a marché
  if (gc) {
    gc->SetPen(*wxBLACK_PEN);
    gc->SetBrush(*wxRED_BRUSH);

    gc->SetPen(*wxBLACK_PEN);
    gc->SetBrush(*wxLIGHT_GREY_BRUSH);
    for (int i = 0; i < partieDuJeu->getHauteur(); i++) {
      for (int j = 0; j < partieDuJeu->getLargeur(); j++) {
        if (partieDuJeu->getCellule(i, j) == 0) {
          gc->SetPen(*wxBLACK_PEN);
          gc->SetBrush(*wxLIGHT_GREY_BRUSH);
          gc->DrawRectangle(leftPadding + (j * TAILLE_CELLULE),
                            topPadding + (i * TAILLE_CELLULE), TAILLE_CELLULE,
                            TAILLE_CELLULE);
        } else {
          gc->SetPen(*wxBLACK_PEN);
          gc->SetBrush(*wxBLACK_BRUSH);
          gc->DrawRectangle(leftPadding + (j * TAILLE_CELLULE),
                            topPadding + (i * TAILLE_CELLULE), TAILLE_CELLULE,
                            TAILLE_CELLULE);
        }
        gc->Flush();
      }
    }
    delete gc;
  }
}

void MyFrame::leftClick(wxMouseEvent& event) {
  // translate coords to ones inside the dc
  wxPoint coords = event.GetPosition();

  // calcule la case dans laquelle le click se situe
  int caseX = (coords.x - leftPadding) / TAILLE_CELLULE;
  int caseY = (coords.y - topPadding) / TAILLE_CELLULE;

  // même pas besoin de vérifier quoi que ce soit, on peut
  // directement envoyer les cases à notre Jeu et il ignorera
  // les requêtes invalides
  partieDuJeu->flipCellule(caseY, caseX);
  Refresh();
}

void MyFrame::OnTimer(wxTimerEvent& event) {
  if (running) {
    partieDuJeu->avance();
    Refresh();
  }
  event.Skip();
}

void MyFrame::OnPause(wxCommandEvent& event) {
  if (running) {
    // on arrete le timer et change l'image du bouton
    running = false;
    toolbar->SetToolNormalBitmap(
      ID_Play, wxBitmap(wxT("../play.png"), wxBITMAP_TYPE_PNG));
  } else {
    // on redemarre le timer et change l'image du bouton
    running = true;
    toolbar->SetToolNormalBitmap(
      ID_Play, wxBitmap(wxT("../pause.png"), wxBITMAP_TYPE_PNG));
  }

  event.Skip();
}

void MyFrame::OnClear(wxCommandEvent& event) {
  partieDuJeu->nettoie();
  Refresh();
  event.Skip();
}

void MyFrame::OnNewOk(wxCommandEvent& event) {
  // on reset la partie avec les nouvelles dimensions
  delete partieDuJeu;
  Grille* magrille = new Grille(newDialog->y_grille, newDialog->x_grille);
  partieDuJeu = new Jeu(magrille);
  Refresh();
  event.Skip();
}

void MyFrame::OnResize(wxSizeEvent& event) { Refresh(); }

void MyFrame::OnSaveAs(wxCommandEvent& event) {
  wxFileDialog saveFileDialog(this, ("Sauvegarder la partie"), "", "",
                              "Fichiers JDLV (*.jdlv)|*.jdlv",
                              wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
  if (saveFileDialog.ShowModal() == wxID_CANCEL)
    // l'utilisateur annule
    return;

  // on sauvegarde
  std::ofstream myfile;
  myfile.open(saveFileDialog.GetPath());
  // si tout va bien, on pousse dans le stream les données de la carte
  // on ecrit la largeur et hauteur
  myfile << partieDuJeu->getLargeur() << "," << partieDuJeu->getHauteur() << "\n";
  // pour chaque ligne
  for (int i = 0; i < partieDuJeu->getHauteur(); i++) {
    // chaque colonne
    for (int j = 0; j < partieDuJeu->getLargeur(); j++) {
      if (partieDuJeu->getCellule(i, j) == 0) {
        // on met un 0
        myfile << "0";
      } else {
        // on met un 1
        myfile <<"1";
      }
    }
    myfile << "\n";
  }
  myfile.close();
}

std::vector<std::string> split (const std::string &s, char delim) {
    std::vector<std::string> result;
    std::stringstream ss(s);
    std::string item;

    while (getline (ss, item, delim)) {
        result.push_back (item);
    }

    return result;
}

void MyFrame::OnOpen(wxCommandEvent& WXUNUSED(event))
{
    
    wxFileDialog openFileDialog(this, _("Ouvrir une partie"), "", "",
                              "Fichiers JDLV (*.jdlv)|*.jdlv", wxFD_OPEN|wxFD_FILE_MUST_EXIST);

    if (openFileDialog.ShowModal() == wxID_CANCEL)
        return;  
    
    // On charge le fichier
    std::ifstream monfichier(openFileDialog.GetPath());

    int largeur = 0;
    int hauteur = 0;
    bool premierLigne = true;
    std::string line;

    int lineIndex=0;

    Grille* magrille;

    if(monfichier.is_open()) {
      while( getline (monfichier, line) ) {
        if(premierLigne) {
          premierLigne = false;
          std::vector<std::string> splitLigne = split(line, ',');
          if(splitLigne.size()!=2) {
            std::cout << "nombre de prefix incorrects:" << splitLigne.size() << std::endl;
            return;
          }
          largeur = std::stoi(splitLigne[0]);
          hauteur = std::stoi(splitLigne[1]);
          // cree une nouvelle grille de la bonne taille
          delete partieDuJeu;
          magrille = new Grille(hauteur, largeur);
          partieDuJeu = new Jeu(magrille);
        } else {
          if(line.length()!=(long unsigned int)largeur) {
            std::cout << "Nombre de ligne incorrect dans le fichier a la ligne "<< lineIndex <<":" << line.length() << std::endl;
            return;
          }
          for(int i=0;i<largeur;i++) {
            if(line[i]=='1')
              magrille->ajouteCellule(lineIndex,i);
          }
          lineIndex++;
          if(lineIndex>=hauteur)break;
        }
      }
      Refresh();
    }

}


wxBEGIN_EVENT_TABLE(MyFrame, wxFrame) 
  EVT_MENU(ID_New, MyFrame::OnNew)
  EVT_MENU(ID_Save, MyFrame::OnSaveAs)
  EVT_MENU(ID_Open, MyFrame::OnOpen)
  EVT_MENU(wxID_EXIT, MyFrame::OnExit) 
  EVT_MENU(wxID_ABOUT, MyFrame::OnAbout)
  EVT_PAINT(MyFrame::OnPaint) 
  EVT_LEFT_DOWN(MyFrame::leftClick)
  EVT_TIMER(TIMER_ID, MyFrame::OnTimer) 
  EVT_TOOL(ID_Play, MyFrame::OnPause)
  EVT_TOOL(ID_Clear, MyFrame::OnClear)
  EVT_BUTTON(ID_New_Ok, MyFrame::OnNewOk) 
  EVT_SIZE(MyFrame::OnResize)
wxEND_EVENT_TABLE()