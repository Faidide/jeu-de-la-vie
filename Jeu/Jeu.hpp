#ifndef JEU_HPP
#define JEU_HPP

#include <fstream>

#ifdef USE_MAKEFILE
#include "../Grille/Grille.hpp"
#else
#include "Grille/Grille.hpp"
#endif // USE_MAKEFILE

class Jeu {
 private:
  // on utilise _grilleBuffer pour faciliter certaines opérations
  Grille* _grilleBuffer;
  Grille* _grille;

 public:
  Jeu(Grille*);
  ~Jeu();
  void avance();
  void affiche() const;
  int getLargeur() const;
  int getHauteur() const;
  bool getCellule(int x, int y) const;
  void flipCellule(int x, int y);
  void nettoie();
};
#endif  // JEU_HPP