#ifndef DEF_FRAME_HPP
#define DEF_FRAME_HPP

#include <wx/wx.h>
#include "NewDialog.h"
#include "../Jeu/Jeu.hpp"

#define TAILLE_CELLULE 20

class MyFrame : public wxFrame {
 public:
  MyFrame(const wxString& title, const wxPoint& pos, const wxSize& size);
  NewDialog *newDialog;
  wxTimer * timer;
  Jeu *partieDuJeu;
  wxToolBar* toolbar;

 private:
  int modalID;
  bool running;
  void OnNew(wxCommandEvent& event);
  void OnExit(wxCommandEvent& event);
  void OnAbout(wxCommandEvent& event);
  void OnPaint(wxPaintEvent& event);
  void OnPause(wxCommandEvent& event);
  void OnClear(wxCommandEvent& event);
  void leftClick(wxMouseEvent& event);
  void OnTimer(wxTimerEvent& event);
  void OnNewOk(wxCommandEvent& event);
  void OnResize(wxSizeEvent& event);
  void OnSaveAs(wxCommandEvent& event);
  void OnOpen(wxCommandEvent& event);

  int taille_x;
  int taille_y;
  int topPadding;
  int leftPadding;
  
  wxDECLARE_EVENT_TABLE();
};

enum { ID_New = 41, ID_Play = 46, ID_Clear = 49, TIMER_ID=51, ID_Save= 1005, ID_Open= 3004  };

#endif // DEF_FRAME_HPP