#ifndef DEF_NEWDIAL_HPP
#define DEF_NEWDIAL_HPP

#include <wx/wx.h>
#include <wx/spinctrl.h>

class NewDialog : public wxDialog {
 public:
  NewDialog(const wxString& title, wxWindow* parent);
  wxButton* exitButton;
  void OnNewClose(wxCommandEvent& event);
  void OnNewOk(wxCommandEvent& event);
  void OnXUpdate(wxSpinEvent& event);
  void OnYUpdate(wxSpinEvent& event);
  // valueur de taille de grille favorite
  int x_grille;
  int y_grille;
  wxSpinCtrl *tcx;
  wxSpinCtrl *tcy;

 private:
  wxWindow* parent;

  wxDECLARE_EVENT_TABLE();
};

enum {
  ID_New_Close = 1002,
  ID_New_Ok = 1003,
  ID_NEW_DIAG = 1005,
  ID_SpinX = 2000,
  ID_SpinY = 2006
};

#endif  // DEF_NEWDIAL_HPP