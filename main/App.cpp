#include "App.h"
#include "Frame.h"

bool MyApp::OnInit() {
  MyFrame* frame =
    new MyFrame("Jeu De la Vie", wxPoint(50, 50), wxSize(450, 340));
  frame->Show(true);
  return true;
}