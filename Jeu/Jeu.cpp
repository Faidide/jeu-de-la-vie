#include "Jeu.hpp"

// On fait pointer notre grille G vers une grille deja existante
Jeu::Jeu(Grille* ptrG) : _grille(ptrG) {
  try {
    // Et on cree une grille de meme taille dans le tas
    _grilleBuffer = new Grille(ptrG->getNbLignes(), ptrG->getNbColonnes());
  } catch (std::bad_alloc& erreur) {
    std::cerr << "Erreur lors de l'allocation de la grille, la mémoire de "
                 "l'ordinateur n'est pas illimitée :)"
              << std::endl;
    throw;
  }
}

Jeu::~Jeu() { delete _grilleBuffer; }

void Jeu::avance() {
  // On verifie qu'il existe une cellule en vie dans le grille
  if (!_grille->estGrilleVivante()) return;

  // On copie les lignes entre ld et lf de G dans _grilleBuffer
  _grilleBuffer->copiePartielle(_grille);

  int nbVoisins;
  // Pour eviter que notre variable d'iteration soit negative sachant qu'elle
  // est de type unsigned
  uint64_t debut;
  if (_grille->getPlusBasseLigneVivante() == 0)
    debut = 0;
  else
    debut = _grille->getPlusBasseLigneVivante() - 1;

  // On parcours entre les lignes ld-1 et lf+1 (ligne debut et ligne fin) pour
  // optimiser,
  for (uint64_t l = debut; l <= _grille->getPlusHauteLigneVivante() + 1; l++)
  // pas de probleme d'indexage commencant a -1 car notre getCase(l,c) nous
  // renvoie une cellule false (morte) si on est a l'exterieur de la grille
  {
    for (uint64_t c = 0; c < _grilleBuffer->getNbColonnes(); c++) {
      // On stocke le nombre de voisins de la cellule (l,c) dans getNbVoisins
      nbVoisins = _grilleBuffer->getNbVoisins(l, c);
      switch (_grilleBuffer->getCase(l, c)) {
        // Cas ou la cellule (l,c) est vivante
        case true:
          if (nbVoisins < 2)
            _grille->supprimeCellule(l, c);
          else if (nbVoisins > 3)
            _grille->supprimeCellule(l, c);
          break;
        // Cas ou la cellule (l,c) est inoccupe
        default:
          if (nbVoisins == 3) _grille->ajouteCellule(l, c);
      }
    }
  }
}

void Jeu::affiche() const { _grille->affiche(); }

int Jeu::getLargeur() const { return _grille->getNbColonnes(); }
int Jeu::getHauteur() const { return _grille->getNbLignes(); }
bool Jeu::getCellule(int x, int y) const {
  if (x < 0 || y < 0) {
    return false;
  }
  return _grille->getCase((uint64_t)x, (uint64_t)y);
}

// tue la cellule si elle est en vie, la réanime si elle est morte
void Jeu::flipCellule(int x, int y) {
  if (_grille->getCase((uint64_t)x, (uint64_t)y)) {
    _grille->supprimeCellule((uint64_t)x, (uint64_t)y);
  } else {
    _grille->ajouteCellule((uint64_t)x, (uint64_t)y);
  }
}

void Jeu::nettoie() {
  for(uint64_t i=0;i<_grille->getNbColonnes();i++) {
    for(uint64_t j=0;j<_grille->getNbLignes(); j++) {
      _grille->supprimeCellule(i,j);
    }
  }
}