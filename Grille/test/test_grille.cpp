#include <gmock/gmock.h>

#include "Grille/Grille.hpp"

namespace {

using testing::Eq;

// test de la creation 
TEST(Grille, creation) {
    // cree une grille
    Grille g(10,40);
    // test le nb de lignes
    ASSERT_THAT(g.getNbLignes(), Eq(10));
    // test le nb de colonnes
    ASSERT_THAT(g.getNbColonnes(), Eq(40));
}

// test de la creation 
TEST(Grille, creationVide) {
    // cree une grille
    Grille g(10,40);
    // test le nb de lignes
    ASSERT_THAT(g.getNbLignes(), Eq(10));
    // test le nb de colonnes
    ASSERT_THAT(g.getNbColonnes(), Eq(40));
    for(int i=0;i<10;i++) {
        for(int j=0;i<40;i++) {
            ASSERT_THAT(g.getCase(i,j), Eq(false));
        }
    }
}

TEST(Grille, setGetCase) {
    // cree une grille
    Grille g(10,40);
    // change la valeur d'une case
    g.ajouteCellule(3,5);
    // verifie que la valeur est respectée
    ASSERT_THAT(g.getCase(3,5), Eq(true));
    ASSERT_THAT(g.getCase(5,3), Eq(false));
}

TEST(Grille, plusHauteOuBasseLigneVivante) {
    // cree une grille
    Grille g(10,40);
    // change la valeur d'une case
    g.ajouteCellule(3,5);
    // verifie que la valeur est respectée
    ASSERT_THAT(g.getPlusBasseLigneVivante(), Eq(3));
    ASSERT_THAT(g.getPlusHauteLigneVivante(), Eq(3));
    g.ajouteCellule(9,5);
    ASSERT_THAT(g.getPlusBasseLigneVivante(), Eq(3));
    ASSERT_THAT(g.getPlusHauteLigneVivante(), Eq(9));
}

TEST(Grille, grilleViergeOuVivante) {
    // cree une grille
    Grille g(10,40);

    // test si la grille est bien vierge et morte
    ASSERT_THAT(g.estGrilleVierge(),Eq(true));
    ASSERT_THAT(g.estGrilleVivante(),Eq(false));

    // change la valeur d'une case
    g.ajouteCellule(3,5);

    ASSERT_THAT(g.estGrilleVierge(),Eq(false));
    ASSERT_THAT(g.estGrilleVivante(),Eq(true));
}

TEST(Grille, copiePartielle) {
    Grille g1(10,10);
    Grille g2(10,10);

    g1.ajouteCellule(7,5);
    g1.ajouteCellule(3,1);

    ASSERT_THAT(g2.getCase(7,5), Eq(false)); 

    g2.copiePartielle(&g1);

    ASSERT_THAT(g2.getCase(7,5), Eq(true));
    ASSERT_THAT(g2.getCase(3,1), Eq(true)); 
}

TEST(Grille, copiePartielleAvecPiege) {
    Grille g1(10,10);
    Grille g2(10,10);

    g1.ajouteCellule(7,5);
    g1.ajouteCellule(3,1);

    ASSERT_THAT(g2.getCase(7,5), Eq(false)); 

    g2.copiePartielle(&g1);

    g1.supprimeCellule(7,5);

    g2.copiePartielle(&g1);

    ASSERT_THAT(g2.getCase(7,5), Eq(false));
    ASSERT_THAT(g2.getCase(3,1), Eq(true));

    ASSERT_THAT(g2.getPlusBasseLigneVivante(), Eq(3));
    ASSERT_THAT(g2.getPlusHauteLigneVivante(), Eq(3));
}

TEST(Grille, nbVoisins1) {
    Grille g1(10,10);
    g1.ajouteCellule(7,5);
    g1.ajouteCellule(8,5);
    g1.ajouteCellule(6,5);
    g1.ajouteCellule(7,4);
    ASSERT_THAT(g1.getNbVoisins(7,5), Eq(3));
    ASSERT_THAT(g1.getNbVoisins(7,4), Eq(3));
    ASSERT_THAT(g1.getNbVoisins(6,5), Eq(2));
    ASSERT_THAT(g1.getNbVoisins(5,6), Eq(1));
}

TEST(Grille, nbVoisins2) {
    Grille g1(10,10);
    g1.ajouteCellule(9,2);
    g1.ajouteCellule(9,1);
    g1.ajouteCellule(8,1);
    g1.ajouteCellule(8,-1);
    ASSERT_THAT(g1.getNbVoisins(9,2), Eq(2));
    ASSERT_THAT(g1.getNbVoisins(9,1), Eq(2));
    ASSERT_THAT(g1.getNbVoisins(8,2), Eq(3));
    ASSERT_THAT(g1.getNbVoisins(9,3), Eq(1));
}

}  // namespace