#include "NewDialog.h"

#include "Frame.h"

NewDialog::NewDialog(const wxString &title, wxWindow *parent)
  : wxDialog(parent, ID_NEW_DIAG, title, wxDefaultPosition, wxSize(250, 170)) {
  wxPanel *panel = new wxPanel(this, -1);

  wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *hbox = new wxBoxSizer(wxHORIZONTAL);

  wxStaticBox *st = new wxStaticBox(panel, -1, wxT("Nouvelle Grille"),
                                    wxPoint(5, 5), wxSize(240, 170));

  // champ pour entrer la taille
  tcx = new wxSpinCtrl(panel, ID_SpinX, wxT(""), wxPoint(165, 35), wxSize(70, 30),
                       wxTE_PROCESS_ENTER, 1, 100, 10, wxT("Largeur"));
  tcy = new wxSpinCtrl(panel, ID_SpinY, wxT(""), wxPoint(165, 85), wxSize(70, 30),
                       wxTE_PROCESS_ENTER, 1, 100, 10, wxT("Hauteur"));

  // text à côté des champs à entrer
  wxStaticText *txtx = new wxStaticText(panel, -1, wxT("largeur:"),
                                        wxPoint(15, 40), wxSize(100, 50));
  wxStaticText *txty = new wxStaticText(panel, -1, wxT("hauteur:"),
                                        wxPoint(15, 90), wxSize(100, 50));

  // boutons
  wxButton *okButton =
    new wxButton(this, ID_New_Ok, wxT("Ok"), wxDefaultPosition, wxSize(70, 30));
  exitButton = new wxButton(this, ID_New_Close, wxT("Fermer"),
                            wxDefaultPosition, wxSize(70, 30));

  // boites d'arrangement
  hbox->Add(okButton, 1);
  hbox->Add(exitButton, 1, wxLEFT, 5);

  vbox->Add(panel, 1);
  vbox->Add(hbox, 0, wxALIGN_CENTER | wxTOP | wxBOTTOM, 10);

  x_grille = 10;
  y_grille = 10;

  SetSizer(vbox);
}

void NewDialog::OnNewClose(wxCommandEvent &event) {
  event.Skip();
  Hide();
}

void NewDialog::OnNewOk(wxCommandEvent &event) {
  // propage l'event au parent
  GetParent()->GetEventHandler()->ProcessEvent(event);
  event.ResumePropagation(999);
  event.Skip();
  Hide();
}

void NewDialog::OnXUpdate(wxSpinEvent &event) { x_grille = tcx->GetValue(); }

void NewDialog::OnYUpdate(wxSpinEvent &event) { y_grille = tcy->GetValue(); }

wxBEGIN_EVENT_TABLE(NewDialog, wxDialog)
  EVT_BUTTON(ID_New_Ok, NewDialog::OnNewOk)
    EVT_BUTTON(ID_New_Close, NewDialog::OnNewClose)
      EVT_SPINCTRL(ID_SpinX, NewDialog::OnXUpdate)
        EVT_SPINCTRL(ID_SpinY, NewDialog::OnYUpdate) wxEND_EVENT_TABLE()