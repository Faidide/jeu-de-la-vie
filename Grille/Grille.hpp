#ifndef GRILLE_HPP
#define GRILLE_HPP
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#include <cstring>
#include <exception>
#include <iostream>

#define NOMBRE_LIGNE_GRILLE_PAR_DEFAUT 100
#define NOMBRE_COLONNE_GRILLE_PAR_DEFAUT 100

// On accède a l'element de la l-ieme ligne et c-ieme colonne de notre attribut
// _grille
// en faisant getCase(l,c).

class Grille {
 private:
  uint64_t _nbLignes;
  uint64_t _nbColonnes;
  // Tableau de bool unidimentionnel (pas du tout optimal de stocker un tableau de bools !!)
  bool* _grille;
  // ces membres sont là pour nous aider à optimiser nos algos
  // le numero de la ligne de la cellule vivante la plus haute de la grille
  uint64_t _plusBasseLigneVivante;
  // le numero de la ligne de la cellule vivante la plus basse de la grille
  uint64_t _plusHauteLigneVivante;
  // Booleen nous indiquant si oui ou non il reste des cellules vivante dans
  // notre grille
  bool _aCellulesVivantes;
  // est-ce la premiere fois qu'on ajoute une cellule ?
  bool _grilleVierge;

  // private setter pour modifier un case de la grille, permet de gérer les dépassements
  // renvoie le nombre de cases modifiées
  int setCase(uint64_t, uint64_t, bool);

  // retourne vrai si la case est occupée
  bool estOccupee(uint64_t l, uint64_t c) const;

 public:
  // Constructeurs
  // Taille par defaut
  Grille();
  // Allocation dans le constructeur d'un bloc de nb_lignes*nb_colonnes octet(s)
  // pointé par l'attribut _grille
  Grille(uint64_t, uint64_t);
  // Constructeur par copie
  Grille(const Grille&);
  // Destructeur
  ~Grille();

  // getters public
  bool estGrilleVierge() const;
  bool estGrilleVivante() const;
  uint64_t getNbLignes() const;
  uint64_t getNbColonnes() const;
  bool getCase(uint64_t, uint64_t) const;
  uint64_t getPlusHauteLigneVivante() const;
  uint64_t getPlusBasseLigneVivante() const;

  // Methode
  // copie la partie de la grille entre les lignes ld et lf inclus
  void copiePartielle(const Grille*);
  void ajouteCellule(uint64_t l, uint64_t c);
  void supprimeCellule(uint64_t l, uint64_t c);
  void affiche() const;
  int getNbVoisins(uint64_t l, uint64_t c) const;
};

#endif // GRILLE_HPP