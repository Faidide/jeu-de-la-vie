# TP3 Jeu de la vie
Par Antoine MILLON et Quentin FAIDIDE.
## Lancer le projet
Pour simplifier le déploiement, le projet main avec wxWidget utilises Makefile, et non pas bazel comme les modules individuels.

Pour lancer l'application:

```bash
cd build
make
./bin/main
```

## Fonctionalités
- Lecture/Pause de l'évolution
- Réinitialisation 
- Enregistrement/Chargement de fichiers .jdlv
- Centrage de la grille
- Ouverture de grilles de tailles arbitraires

## Mises en gardes
- Nous n'avons pas utilisé les icones systèmes, par conséquents, si vous utilisez un thème sombre, il se peut que vous obteniez des icônes sombres sur un fond sombre.
- Veuillez à bien lancer le main depuis le dossier build pour arriver à trouver les ressources.
- Le fenêtrage n'a été testé qu'avec un gestionnaire de fenêtres en tuile (i3), par conséquent, la taille par défaut ainsi que certains redimensionnement n'ont pas été énormément testés.

## Pourquoi pas Bazel
Bazel nous force à packager les librairies dans des "libraires virtuelles" car il éxécute la compilation et les tests dans des sandboxs, par conséquent, après DES HEURES de bricolages, et après avoir constaté que les inclusions dépendraient de l'OS de l'utilisateur, on a choisir de passer au dernier moment à un Makefile.

Désolé pour la structure Hybride, au moins au saura maintenant qu'il vaut mieux un Makefile quand on utilise des libraires non statiques.
