#include "Grille.hpp"

// getters de la classe Grille
uint64_t Grille::getNbLignes() const { return _nbLignes; }
uint64_t Grille::getNbColonnes() const { return _nbColonnes; }
uint64_t Grille::getPlusHauteLigneVivante() const {
  return _plusHauteLigneVivante;
}
uint64_t Grille::getPlusBasseLigneVivante() const {
  return _plusBasseLigneVivante;
}
bool Grille::estGrilleVivante() const { return _aCellulesVivantes; }
bool Grille::estGrilleVierge() const { return _grilleVierge; }

// Les constantes sont definies dans le Grille.h par des #define
Grille::Grille()
  : _nbLignes(NOMBRE_LIGNE_GRILLE_PAR_DEFAUT),
    _nbColonnes(NOMBRE_COLONNE_GRILLE_PAR_DEFAUT) {
  try {
    // Allocation de nb_lignes*nb_colonnes octets en mémoire dans le tas et
    // initialisation de la zone mémoire a 0
    _grille = new bool[NOMBRE_LIGNE_GRILLE_PAR_DEFAUT *
                       NOMBRE_COLONNE_GRILLE_PAR_DEFAUT]();
  } catch (std::bad_alloc& erreur) {
    std::cerr << "Erreur lors de l'allocation de la grille, la mémoire de "
            "l'ordinateur n'est pas illimitée :)"
         << std::endl;
    // on passe l'erreur pour qu'elle cascade vers un éventuel catch plus haut
    throw;
  }
  _plusBasseLigneVivante = 0;
  _plusHauteLigneVivante = 0;
  _grilleVierge = true;
  _aCellulesVivantes=false;
}

Grille::Grille(uint64_t nb_lignes, uint64_t nb_colonnes)
  : _nbLignes(nb_lignes), _nbColonnes(nb_colonnes) {
  try {
    // Allocation de nb_lignes*nb_colonnes octets en mémoire dans le tas et
    // initialisation de la zone mémoire a 0
    _grille = new bool[nb_lignes * nb_colonnes]();
  } catch (std::bad_alloc& erreur) {
      std::cerr << "Erreur d'allocation mémoire" << std::endl;
      throw;
  }
  _plusBasseLigneVivante = 0;
  _plusHauteLigneVivante = 0;
  _grilleVierge=true;
  _aCellulesVivantes=false;
}

// Constructeur par copie
Grille::Grille(const Grille& G) : Grille(G.getNbLignes(), G.getNbColonnes()) {
  // On recopie la zone memoire de la grille entre les lignes ld et lf inclus
  memcpy(_grille + G._plusBasseLigneVivante * getNbColonnes(),
         G._grille + G._plusBasseLigneVivante * getNbColonnes(),
         (G._plusHauteLigneVivante - G._plusBasseLigneVivante + 1) * getNbColonnes());

  // On recopie les elements qui nous servent a optimiser les acces
  _plusBasseLigneVivante = G._plusBasseLigneVivante;
  _plusHauteLigneVivante = G._plusHauteLigneVivante;
  _aCellulesVivantes=G.estGrilleVivante();
  _grilleVierge=G.estGrilleVierge();
}

Grille::~Grille() {
  // On désalloue la memoire dans le tas
  delete[] _grille;
}

bool Grille::getCase(uint64_t l, uint64_t c) const {
  // Toute cellule hors grille est considéré comme non vivante
  if (!(l >= 0 && l < getNbLignes()) || !(c >= 0 && c < getNbColonnes())) return false;
  return _grille[getNbColonnes() * l + c];
}

// retourne le nombre de cases modifiés
// ATTENTION: ne modifie pas les numeros de ligne entre lesquel la grille est vivante !
int Grille::setCase(uint64_t l, uint64_t c, bool valeur) {
  // Cas d'une cellule hors grille
  if (!(l >= 0 && l < getNbLignes()) || !(c >= 0 && c < getNbColonnes())) {
    // On ne fait rien si on cherche a modifier une case hors grille, c'est
    // notre regle
    return 0;
  }
  _grille[getNbColonnes() * l + c] = valeur;
  return 1;
}

void Grille::ajouteCellule(uint64_t l, uint64_t c) {
  // On ajoute une cellule a la ligne l et la colonne c de _grille.
  int nbCasesAjoutees = setCase(l, c, true);
  // on vérifie que l'ajoute s'est bien passé
  if(nbCasesAjoutees!=0) {
    // On est sur que la grille a une cellule vivante ici
    _aCellulesVivantes=true;
    // Mise a jour GrilleInfo
    // Primordiale pour l'initialisation de ld et lf
    if (_grilleVierge) {
        _plusBasseLigneVivante = l;
        _plusHauteLigneVivante = l;
        _grilleVierge = false;
    } else {
        if (l < _plusBasseLigneVivante)
        _plusBasseLigneVivante = l;
        else if (l > _plusHauteLigneVivante)
        _plusHauteLigneVivante = l;
    }
  }
}

void Grille::supprimeCellule(uint64_t l, uint64_t c) {
  // On supprime une cellule a la ligne l et la colonne c de _grille.
  setCase(l, c, false);

  // Mise a jour GrilleInfo
  if (l != _plusBasseLigneVivante && l != _plusHauteLigneVivante) return;
  // Cas ou on doit modifier la valeur de ld(ligne debut)
  if (l == _plusBasseLigneVivante) {
    for (uint64_t l = _plusBasseLigneVivante; l <= _plusHauteLigneVivante;
         l++) {
      for (uint64_t c = 0; c < getNbColonnes(); c++) {
        if (estOccupee(l, c)) {
          _plusBasseLigneVivante = l;
          return;
        }
      }
    }
  }
  // Cas ou on doit modifier la valeur de lf(ligne fin)
  else if (l == _plusHauteLigneVivante) {
    for (uint64_t l = _plusHauteLigneVivante; l >= _plusBasseLigneVivante;
         l--) {
      for (uint64_t c = 0; c < getNbColonnes(); c++) {
        if (estOccupee(l, c)) {
          _plusHauteLigneVivante = l;
          return;
        }
      }
    }
  }
  // Cas ou on vient de supprimer la dernière cellule de notre grille
  _aCellulesVivantes=false;
  return;
}

// copie la partie de la grille entre les lignes la haute vivante et la plus basse
void Grille::copiePartielle(const Grille* G) {
  if (getNbLignes() != G->getNbLignes() || getNbColonnes() != G->getNbColonnes()) {
    throw std::runtime_error("Copier deux grilles de tailles différentes n'est pas autorisé.");
  }
  // On reinitialise la grille
  memset(_grille, 0, getNbColonnes() * getNbLignes());
  _plusBasseLigneVivante = 0;
  _plusHauteLigneVivante = 0;
  // si la grille est pas morte
  if (G->estGrilleVivante()) {
    // On copie la partie "utile" de G dans l'objet
    memcpy(
      _grille + G->_plusBasseLigneVivante * getNbColonnes(),
      G->_grille + G->_plusBasseLigneVivante * getNbColonnes(),
      (G->_plusHauteLigneVivante - G->_plusBasseLigneVivante + 1) * getNbColonnes());
    // on copie les nouveau numeros de lignes entre lesquels on a du vivant
    _plusBasseLigneVivante = G->_plusBasseLigneVivante;
    _plusHauteLigneVivante = G->_plusHauteLigneVivante;
  }
}

void Grille::affiche() const {
  for (uint64_t l = 0; l < getNbLignes(); l++) {
    for (uint64_t c = 0; c < getNbColonnes(); c++) {
      std::cout << getCase(l, c);
    }
    std::cout << std::endl;
  }
}

bool Grille::estOccupee(uint64_t l, uint64_t c) const {
  // Renvoie true si la cellule (l,c) est vivante false sinon
  return (getCase(l, c) == true);
}

// Renvoie le nombre de cellule adjacente vivante a la cellule (l,c)
int Grille::getNbVoisins(uint64_t l, uint64_t c) const {
  int nb_voisins = 0;
  for (int8_t i = -1; i < 2; i++) {
    for (int8_t j = -1; j < 2; j++) {
      // On ne compte pas la cellule (l,c) et de base les cellules hors grille
      // sont non vivante, cf getCase().
      if ((estOccupee(l + i, c + j)) && !(l + i == l && c + j == c)) {
        nb_voisins++;
      }
    }
  }
  return nb_voisins;
}
